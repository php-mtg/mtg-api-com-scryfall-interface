<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

/**
 * ApiComScryfallPrices class file.
 * 
 * An object containing daily price information for this card, including usd,
 * usd_foil, eur, and tix prices, as strings.
 * 
 * @author Anastaszor
 */
class ApiComScryfallPrices
{
	
	/**
	 * The value of the card, in usd.
	 * 
	 * @var ?float
	 */
	public ?float $usd = null;
	
	/**
	 * The value of the foil card, in usd.
	 * 
	 * @var ?float
	 */
	public ?float $usdFoil = null;
	
	/**
	 * The value of the etched card, if exists, in usd.
	 * 
	 * @var ?float
	 */
	public ?float $usdEtched = null;
	
	/**
	 * The value of the glossy card, if exists, in usd.
	 * 
	 * @var ?float
	 */
	public ?float $usdGlossy = null;
	
	/**
	 * The value of the card, in eur.
	 * 
	 * @var ?float
	 */
	public ?float $eur = null;
	
	/**
	 * The value of the card, in eur foil.
	 * 
	 * @var ?float
	 */
	public ?float $eurFoil = null;
	
	/**
	 * The value of the card, in tix.
	 * 
	 * @var ?float
	 */
	public ?float $tix = null;
	
}
