<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallExtension class file.
 * 
 * This class represents a set from the scryfall api.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComScryfallExtension
{
	
	/**
	 * A unique ID for this set on Scryfall that will not change.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $id = null;
	
	/**
	 * The unique three to five-letter code for this set. 
	 * 
	 * @var ?string
	 */
	public ?string $code = null;
	
	/**
	 * The unique code for this set on MTGO, which may differ from the regular code. 
	 * 
	 * @var ?string
	 */
	public ?string $mtgoCode = null;
	
	/**
	 * The arena code.
	 * 
	 * @var ?string
	 */
	public ?string $arenaCode = null;
	
	/**
	 * This set’s ID on TCGplayer’s API, also known as the groupId.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerId = null;
	
	/**
	 * The English name of the set.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * A computer-readable classification for this set. 
	 * 
	 * @var ?string
	 */
	public ?string $setType = null;
	
	/**
	 * The date the set was released or the first card was printed in the set
	 * (in GMT-8 Pacific time). 
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releasedAt = null;
	
	/**
	 * The block code for this set, if any.
	 * 
	 * @var ?string
	 */
	public ?string $blockCode = null;
	
	/**
	 * The block or group name code for this set, if any.
	 * 
	 * @var ?string
	 */
	public ?string $block = null;
	
	/**
	 * The set code for the parent set, if any. promo and token sets often have
	 * a parent set.
	 * 
	 * @var ?string
	 */
	public ?string $parentSetCode = null;
	
	/**
	 * The printed size of this extension.
	 * 
	 * @var ?integer
	 */
	public ?int $printedSize = null;
	
	/**
	 * The number of cards in this set.
	 * 
	 * @var ?integer
	 */
	public ?int $cardCount = null;
	
	/**
	 * True if this set was only released on Magic Online.
	 * 
	 * @var ?boolean
	 */
	public ?bool $digital = null;
	
	/**
	 * True if this set contains only foil cards.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foilOnly = null;
	
	/**
	 * True if this set contains only nonfoil cards.
	 * 
	 * @var ?boolean
	 */
	public ?bool $nonfoilOnly = null;
	
	/**
	 * A link to this set’s permapage on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $scryfallUri = null;
	
	/**
	 * A link to this set object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $uri = null;
	
	/**
	 * A URI to an SVG file for this set’s icon on Scryfall’s CDN. Hotlinking
	 * this image isn’t recommended, because it may change slightly over time.
	 * You should download it and use it locally for your particular user
	 * interface needs. 
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $iconSvgUri = null;
	
	/**
	 * A Scryfall API URI that you can request to begin paginating over the
	 * cards in this set. 
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $searchUri = null;
	
}
