<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

/**
 * ApiComScryfallObject class file.
 * 
 * This class is a generic object for all classes of this api.
 * 
 * @author Anastaszor
 */
class ApiComScryfallError
{
	
	/**
	 * The detailed error.
	 * 
	 * @var ?string
	 */
	public ?string $details = null;
	
	/**
	 * The error http code.
	 * 
	 * @var ?integer
	 */
	public ?int $status = null;
	
	/**
	 * The type error.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
	/**
	 * The warnings for this object.
	 * 
	 * @var array<integer, string>
	 */
	public array $warnings = [];
	
}
