<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiComScryfallEndpoint class file.
 *
 * This class is the entry point for all methods using this api.
 *
 * @author Anastaszor
 */
interface ApiComScryfallEndpointInterface extends Stringable
{
	
	/**
	 * Gets the list of all extensions.
	 * 
	 * @return ApiComScryfallPagination<ApiComScryfallExtension>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSets() : ApiComScryfallPagination;
	
	/**
	 * Gets the extension for given code.
	 * 
	 * @param string $setCode
	 * @return ApiComScryfallExtension
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByCode(string $setCode) : ApiComScryfallExtension;
	
	/**
	 * Gets the extension for the given tcgplayer group id.
	 * 
	 * @param integer $groupId
	 * @return ApiComScryfallExtension
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByTcgplayerId(int $groupId) : ApiComScryfallExtension;
	
	/**
	 * Gets the extension for the given uuid.
	 * 
	 * @param UuidInterface $setId
	 * @return ApiComScryfallExtension
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetByScryfallId(UuidInterface $setId) : ApiComScryfallExtension;
	
	/**
	 * Gets the cards with the given query.
	 * 
	 * @param integer $page
	 * 
	 * @param ?string $query see https://scryfall.com/docs/syntax
	 * 
	 * @param ?string $unique The strategy for omitting similar cards.
	 *                        The unique parameter specifies if Scryfall should remove “duplicate”
	 *                        results in your query. The options are:
	 *                        cards		Removes duplicate gameplay objects (cards that share a
	 *                        name and have the same functionality). For example, if
	 *                        your search matches more than one print of Pacifism,
	 *                        only one copy of Pacifism will be returned.
	 *                        art			Returns only one copy of each unique artwork for
	 *                        matching cards. For example, if your search matches
	 *                        more than one print of Pacifism, one card with each
	 *                        different illustration for Pacifism will be returned,
	 *                        but any cards that duplicate artwork already in the
	 *                        results will be omitted.
	 *                        prints		Returns all prints for all cards matched (disables
	 *                        rollup). For example, if your search matches more than
	 *                        one print of Pacifism, all matching prints will be
	 *                        returned.
	 * 
	 * @param ?string $order The method to sort returned cards.
	 *                       The order parameter determines how Scryfall should sort the
	 *                       returned cards :
	 *                       name		Sort cards by name, A → Z
	 *                       set			Sort cards by their set and collector number: AAA/#1 → ZZZ/#999
	 *                       released	Sort cards by their release date: Newest → Oldest
	 *                       rarity		Sort cards by their rarity: Common → Mythic
	 *                       color		Sort cards by their color and color identity: WUBRG → multicolor → colorless
	 *                       usd			Sort cards by their lowest known U.S. Dollar price: 0.01 → highest, null last
	 *                       tix			Sort cards by their lowest known TIX price: 0.01 → highest, null last
	 *                       eur			Sort cards by their lowest known Euro price: 0.01 → highest, null last
	 *                       cmc			Sort cards by their converted mana cost: 0 → highest
	 *                       power		Sort cards by their power: null → highest
	 *                       toughness	Sort cards by their toughness: null → highest
	 *                       edhrec		Sort cards by their EDHREC ranking: lowest → highest
	 *                       artist		Sort cards by their front-side artist name: A → Z
	 * 
	 * @param ?string $dir The direction to sort cards.
	 *                     auto	Scryfall will automatically choose the most inuitive direction to sort
	 *                     asc		Sort ascending (the direction of the arrows in the previous table)
	 *                     desc	Sort descending (flip the direction of the arrows in the previous table)
	 * 
	 * @param bool $includeExtras If true, extra cards (tokens, planes, etc)
	 *                            will be included. Equivalent to adding include:extras to the
	 *                            fulltext search. Defaults to false.
	 * 
	 * @param bool $includeMultilingual If true, cards in every language
	 *                                  supported by Scryfall will be included. Defaults to false.
	 * 
	 * @param bool $includeVariations If true, rare care variants will be
	 *                                included, like the Hairy Runesword. Defaults to false.
	 * 
	 * @return ApiComScryfallPagination<ApiComScryfallCard>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardsSearch(int $page = 1, ?string $query = null, ?string $unique = null, ?string $order = null, ?string $dir = null, bool $includeExtras = false, bool $includeMultilingual = false, bool $includeVariations = false) : ApiComScryfallPagination;
	
	/**
	 * Gets the cards with the given search query.
	 * 
	 * @param string $name the card name to search for
	 * @param boolean $fuzzy whether to do fuzzy search
	 * @param string $set a set code to limit the search to one set
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardNamed(string $name, bool $fuzzy = false, ?string $set = null) : ?ApiComScryfallCard;
	
	/**
	 * Gets a list of the 20 next cards for the autocomplete.
	 * 
	 * @param string $query
	 * @param boolean $includeExtras
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardsAutocomplete(string $query, bool $includeExtras = false) : ApiComScryfallCatalog;
	
	/**
	 * Gets a single random card object.
	 * 
	 * @param string $query
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardRandom(?string $query = null) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given collection number in the given set, for
	 * the given language (en by default).
	 * 
	 * @param string $setCode
	 * @param string $collNb
	 * @param string $lang
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCard(string $setCode, string $collNb, ?string $lang = null) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given multiverse id.
	 * 
	 * @param integer $multiverseId
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByMultiverseId(int $multiverseId) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given mtgo id.
	 * 
	 * @param integer $mtgoId
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByMtgoId(int $mtgoId) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given arena id.
	 * 
	 * @param integer $arenaId
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByArenaId(int $arenaId) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given tcgplayer id.
	 * 
	 * @param integer $tcgpId
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByTcgplayerId(int $tcgpId) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given cardmarket id.
	 *
	 * @param integer $mcmId
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByCardmarketId(int $mcmId) : ApiComScryfallCard;
	
	/**
	 * Gets the card with the given scryfall uuid.
	 * 
	 * @param UuidInterface $uuid
	 * @return ApiComScryfallCard
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardByScryfallId(UuidInterface $uuid) : ApiComScryfallCard;
	
	/**
	 * Downloads the image for the given card.
	 * 
	 * @param UuidInterface $cardId
	 * @param string $format "small", "normal", "large", "png", "art_crop", or
	 *                       "border_crop". Defaults to "large".
	 * @param boolean $back true if the back should be downloaded, false = face
	 * @return string the binary data
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardImage(UuidInterface $cardId, string $format = 'large', bool $back = false) : string;
	
	/**
	 * Gets the rulings of a specific card with its multiverse id.
	 * 
	 * @param integer $multiverseId
	 * @return ApiComScryfallPagination<ApiComScryfallRuling>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRulingsFromMultiverseId(int $multiverseId) : ApiComScryfallPagination;
	
	/**
	 * Gets the rulings of a specific card with its mtgo id.
	 * 
	 * @param integer $mtgoId
	 * @return ApiComScryfallPagination<ApiComScryfallRuling>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRulingsFromMtgoId(int $mtgoId) : ApiComScryfallPagination;
	
	/**
	 * Gets the rulings of a specific card with its mtg arena id.
	 * 
	 * @param integer $arenaId
	 * @return ApiComScryfallPagination<ApiComScryfallRuling>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRulingsFromArenaId(int $arenaId) : ApiComScryfallPagination;
	
	/**
	 * Gets the rulings of a specific card with its extension code and card
	 * number.
	 * 
	 * @param string $extCode
	 * @param string $cardNb
	 * @return ApiComScryfallPagination<ApiComScryfallRuling>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRulingsFromCard(string $extCode, string $cardNb) : ApiComScryfallPagination;
	
	/**
	 * Gets the ruling of a specific card with its scryfall id.
	 * 
	 * @param UuidInterface $scryfallCardId
	 * @return ApiComScryfallPagination<ApiComScryfallRuling>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRulingsFromScryfallId(UuidInterface $scryfallCardId) : ApiComScryfallPagination;
	
	/**
	 * Gets a list of all existing symbols used by magic cards.
	 * 
	 * @return ApiComScryfallPagination<ApiComScryfallSymbol>
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSymbology() : ApiComScryfallPagination;
	
	/**
	 * Gets the parsed mana cost that this api understands from the given cost
	 * line.
	 * 
	 * @param string $cost
	 * @return ApiComScryfallParsedMana
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getParsedMana(string $cost) : ApiComScryfallParsedMana;
	
	/**
	 * Gets the catalog of card names.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCardNames() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of the artist names.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArtistNames() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of the word bank.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getWordBank() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of creature types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCreatureTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of planeswalker types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPlaneswalkerTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of land types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getLandTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of artifact types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArtifactTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of enchantment types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getEnchantmentTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of spell types.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSpellTypes() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of creature powers.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPowers() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of creature toughnesses.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getToughnesses() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of planeswalker loyalties.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getLoyalties() : ApiComScryfallCatalog;
	
	/**
	 * Gets the catalog of card watermarks.
	 * 
	 * @return ApiComScryfallCatalog
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getWatermarks() : ApiComScryfallCatalog;
	
}
