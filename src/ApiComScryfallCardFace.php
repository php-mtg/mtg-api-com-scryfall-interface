<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComScryfallCardFace class file.
 * 
 * This class represents a face of a given card from the scryfall api.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComScryfallCardFace
{
	
	/**
	 * The id of the illustrator of this card face.
	 * 
	 * @var ?string
	 */
	public ?string $artistId = null;
	
	/**
	 * The name of the illustrator of this card face. Newly spoiled cards may
	 * not have this field yet.
	 * 
	 * @var ?string
	 */
	public ?string $artist = null;
	
	/**
	 * The colors in this face’s color indicator, if any.
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIndicator = [];
	
	/**
	 * This face’s colors, if the game defines colors for the individual face
	 * of this card.
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * The flavor name printed on this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $flavorName = null;
	
	/**
	 * The flavor text printed on this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $flavorText = null;
	
	/**
	 * A unique identifier for the card face artwork that remains consistent
	 * across reprints. Newly spoiled cards may not have this field yet.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $illustrationId = null;
	
	/**
	 * An object providing URIs to imagery for this face, if this is a
	 * double-sided card. If this card is not double-sided, then the image_uris
	 * property will be part of the parent object instead.
	 * 
	 * @var array<integer, string>
	 */
	public array $imageUris = [];
	
	/**
	 * This face’s loyalty, if any.
	 * 
	 * @var ?string
	 */
	public ?string $loyalty = null;
	
	/**
	 * The mana cost for this face. This value will be any empty string "" if
	 * the cost is absent. Remember that per the game rules, a missing mana
	 * cost and a mana cost of {0} are different values.
	 * 
	 * @var ?string
	 */
	public ?string $manaCost = null;
	
	/**
	 * The name of this particular face.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The Oracle text for this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $oracleText = null;
	
	/**
	 * This face’s power, if any. Note that some cards have powers that are not
	 * numeric, such as *.
	 * 
	 * @var ?string
	 */
	public ?string $power = null;
	
	/**
	 * The localized name printed on this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedName = null;
	
	/**
	 * The localized text printed on this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedText = null;
	
	/**
	 * The localized type line printed on this face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedTypeLine = null;
	
	/**
	 * This face’s toughness, if any.
	 * 
	 * @var ?string
	 */
	public ?string $toughness = null;
	
	/**
	 * The type line of this particular face.
	 * 
	 * @var ?string
	 */
	public ?string $typeLine = null;
	
	/**
	 * The watermark on this particulary card face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $watermark = null;
	
}
