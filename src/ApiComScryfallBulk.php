<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallBulk class file.
 * 
 * This class represents the bulk data metadata.
 * 
 * @author Anastaszor
 */
class ApiComScryfallBulk
{
	
	/**
	 * A unique ID for this bulk item.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $id = null;
	
	/**
	 * A human-readable name for this file.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The type of bulk data.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
	/**
	 * A human-readable description for this file.
	 * 
	 * @var ?string
	 */
	public ?string $description = null;
	
	/**
	 * The URL that hosts this bulk file.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $permalinkUri = null;
	
	/**
	 * The time when this file was last updated.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $updatedAt = null;
	
	/**
	 * The size of this file in integer bytes.
	 * 
	 * @var ?integer
	 */
	public ?int $size = null;
	
	/**
	 * The MIME type of this file.
	 * 
	 * @var ?string
	 */
	public ?string $contentType = null;
	
	/**
	 * The Content-Encoding encoding that will be used to transmit this file
	 * when you download it.
	 * 
	 * @var ?string
	 */
	public ?string $contentEncoding = null;
	
}
