<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

/**
 * ApiComScryfallCardLegality class file.
 * 
 * This class represents the different legality statuses of this card in the
 * different formats.
 * 
 * Possible legalities are "legal", "not_legal", "restricted", and "banned".
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComScryfallCardLegality
{
	
	/**
	 * The legality for 1v1.
	 * 
	 * @var ?string
	 */
	public ?string $oneVone = null;
	
	/**
	 * The legality for alchemy.
	 * 
	 * @var ?string
	 */
	public ?string $alchemy = null;
	
	/**
	 * The legality for brawl.
	 * 
	 * @var ?string
	 */
	public ?string $brawl = null;
	
	/**
	 * The legality for commander.
	 * 
	 * @var ?string
	 */
	public ?string $commander = null;
	
	/**
	 * The legality for duel commander.
	 * 
	 * @var ?string
	 */
	public ?string $duel = null;
	
	/**
	 * The legality for explorer.
	 * 
	 * @var ?string
	 */
	public ?string $explorer = null;
	
	/**
	 * The legality for frontier.
	 * 
	 * @var ?string
	 */
	public ?string $frontier = null;
	
	/**
	 * The legality for future.
	 * 
	 * @var ?string
	 */
	public ?string $future = null;
	
	/**
	 * The legality for gladiator.
	 * 
	 * @var ?string
	 */
	public ?string $gladiator = null;
	
	/**
	 * The legality for historic.
	 * 
	 * @var ?string
	 */
	public ?string $historic = null;
	
	/**
	 * The legality for historic brawl.
	 * 
	 * @var ?string
	 */
	public ?string $historicbrawl = null;
	
	/**
	 * The legality for legacy.
	 * 
	 * @var ?string
	 */
	public ?string $legacy = null;
	
	/**
	 * The legality for modern.
	 * 
	 * @var ?string
	 */
	public ?string $modern = null;
	
	/**
	 * The legality of the oathbreaker format.
	 * 
	 * @var ?string
	 */
	public ?string $oathbreaker = null;
	
	/**
	 * The legality for oldschool format.
	 * 
	 * @var ?string
	 */
	public ?string $oldschool = null;
	
	/**
	 * The legality for pauper.
	 * 
	 * @var ?string
	 */
	public ?string $pauper = null;
	
	/**
	 * The legality for pauper commander.
	 * 
	 * @var ?string
	 */
	public ?string $paupercommander = null;
	
	/**
	 * The legality for penny.
	 * 
	 * @var ?string
	 */
	public ?string $penny = null;
	
	/**
	 * The legality for pioneer.
	 * 
	 * @var ?string
	 */
	public ?string $pioneer = null;
	
	/**
	 * The legality of the predh format.
	 * 
	 * @var ?string
	 */
	public ?string $predh = null;
	
	/**
	 * The legality for premodern.
	 * 
	 * @var ?string
	 */
	public ?string $premodern = null;
	
	/**
	 * The legality for standard.
	 * 
	 * @var ?string
	 */
	public ?string $standard = null;
	
	/**
	 * The legality for vintage.
	 * 
	 * @var ?string
	 */
	public ?string $vintage = null;
	
}
