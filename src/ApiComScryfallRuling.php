<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComScryfallRuling class file.
 * 
 * This class represents a ruling about a card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallRuling
{
	
	/**
	 * The id of the oracle object.
	 * 
	 * @var UuidInterface
	 */
	public ?UuidInterface $oracleId = null;
	
	/**
	 * The source of this ruling.
	 * 
	 * @var ?string
	 */
	public ?string $source = null;
	
	/**
	 * The date of publication of this ruling.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $publishedAt = null;
	
	/**
	 * The text of this ruling.
	 * 
	 * @var ?string
	 */
	public ?string $comment = null;
	
}
