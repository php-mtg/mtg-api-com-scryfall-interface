<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallRelatedCard class file.
 * 
 * This class represents the related card links to this card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallRelatedCard
{
	
	/**
	 * An unique ID for this card in Scryfall’s database.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $id = null;
	
	/**
	 * A field explaining what role this card plays in this relationship, one
	 * of token, meld_part, meld_result, or combo_piece.
	 * 
	 * @var ?string
	 */
	public ?string $component = null;
	
	/**
	 * The name of this particular related card.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The type line of this card.
	 * 
	 * @var ?string
	 */
	public ?string $typeLine = null;
	
	/**
	 * A URI where you can retrieve a full object describing this card on
	 * Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $uri = null;
	
}
