<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

/**
 * ApiComScryfallPagination class file.
 * 
 * This class represents a pagination from the scryfall api.
 * 
 * @author Anastaszor
 * @template T of object
 */
class ApiComScryfallPagination
{
	
	/**
	 * Whether this collection has more pages.
	 * 
	 * @var ?boolean
	 */
	public ?bool $hasMore = null;
	
	/**
	 * The next page of this collection. 
	 * 
	 * @var ?integer
	 */
	public ?int $nextPage = null;
	
	/**
	 * The total cards of this collection.
	 * 
	 * @var ?integer
	 */
	public ?int $totalCards = null;
	
	/**
	 * The actual data in the collection.
	 * 
	 * @var array<integer, T>
	 */
	public array $data = [];
	
}
