<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallCardResource class file.
 * 
 * This class represents other resources for the card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallCardResource
{
	
	/**
	 * The url to the gatherer page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $gatherer = null;
	
	/**
	 * The url to the tcgplayer decks page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayerDecks = null;
	
	/**
	 * The url to the tcgplayer article page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayerInfiniteArticles = null;
	
	/**
	 * The url to the tcgplayer decks page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayerInfiniteDecks = null;
	
	/**
	 * The url to the edhrec page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $edhrec = null;
	
	/**
	 * The url to the mtgtop8 page of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $mtgtop8 = null;
	
}
