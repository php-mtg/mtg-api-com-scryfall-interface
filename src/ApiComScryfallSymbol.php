<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallSymbol class file.
 * 
 * This class represents a symbol.
 * 
 * @author Anastaszor
 */
class ApiComScryfallSymbol
{
	
	/**
	 * The plaintext symbol. Often surrounded with curly braces {}. Note that
	 * not all symbols are ASCII text (for example, {∞}).
	 * 
	 * @var ?string
	 */
	public ?string $symbol = null;
	
	/**
	 * An alternate version of this symbol, if it is possible to write it
	 * without curly braces.
	 * 
	 * @var ?string
	 */
	public ?string $looseVariant = null;
	
	/**
	 * An English snippet that describes this symbol. Appropriate for use in
	 * alt text or other accessible communication formats.
	 * 
	 * @var ?string
	 */
	public ?string $english = null;
	
	/**
	 * True if it is possible to write this symbol “backwards”. For example,
	 * the official symbol {U/P} is sometimes written as {P/U} or {P\U} in
	 * informal settings. Note that the Scryfall API never writes symbols
	 * backwards in other responses. This field is provided for informational
	 * purposes.
	 * 
	 * @var ?boolean
	 */
	public ?bool $transposable = null;
	
	/**
	 * True if this is a mana symbol.
	 * 
	 * @var ?boolean
	 */
	public ?bool $representsMana = null;
	
	/**
	 * A decimal number representing this symbol’s converted mana cost. Note
	 * that mana symbols from funny sets can have fractional converted mana
	 * costs.
	 * 
	 * @var ?float
	 */
	public ?float $cmc = null;
	
	/**
	 * True if this symbol appears in a mana cost on any Magic card. For
	 * example {20} has this field set to false because {20} only appears in
	 * Oracle text, not mana costs.
	 * 
	 * @var ?boolean
	 */
	public ?bool $appearsInManaCosts = null;
	
	/**
	 * True if this symbol is only used on funny cards or Un-cards.
	 * 
	 * @var ?boolean
	 */
	public ?bool $funny = null;
	
	/**
	 * An array of colors that this symbol represents.
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * An array of plaintext versions of this symbol that Gatherer uses on old
	 * cards to describe original printed text. For example: {W} has ["oW",
	 * "ooW"] as alternates.
	 * 
	 * @var array<integer, string>
	 */
	public array $gathererAlternates = [];
	
	/**
	 * The mana value of the symbol.
	 * 
	 * @var ?float
	 */
	public ?float $manaValue = null;
	
	/**
	 * The uri for the svg file.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $svgUri = null;
	
}
