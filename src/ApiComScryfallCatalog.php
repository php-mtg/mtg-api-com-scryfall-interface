<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallCatalog class file.
 * 
 * This class represents a catalog of terms.
 * 
 * @author Anastaszor
 */
class ApiComScryfallCatalog
{
	
	/**
	 * The uri of current catalog.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $uri = null;
	
	/**
	 * The total values of this catalog.
	 * 
	 * @var ?integer
	 */
	public ?int $totalValues = null;
	
	/**
	 * The data of this catalog.
	 * 
	 * @var array<integer, string>
	 */
	public array $data = [];
	
}
