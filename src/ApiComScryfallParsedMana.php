<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

/**
 * ApiComScryfallParsedMana class file.
 * 
 * This class represents the parsed mana response that the api has understood.
 * 
 * @author Anastaszor
 */
class ApiComScryfallParsedMana
{
	
	/**
	 * The normalized cost, with correctly-ordered and wrapped mana symbols.
	 * 
	 * @var ?string
	 */
	public ?string $cost = null;
	
	/**
	 * The converted mana cost. If you submit Un-set mana symbols, this decimal
	 * could include fractional parts.
	 * 
	 * @var ?float
	 */
	public ?float $cmc = null;
	
	/**
	 * The colors of the given cost.
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * True if the cost is colorless.
	 * 
	 * @var ?boolean
	 */
	public ?bool $colorless = null;
	
	/**
	 * True if the cost is monocolored.
	 * 
	 * @var ?boolean
	 */
	public ?bool $monocolored = null;
	
	/**
	 * True if the cost is multicolored.
	 * 
	 * @var ?boolean
	 */
	public ?bool $multicolored = null;
	
}
