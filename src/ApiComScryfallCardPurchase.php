<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallCardPurchase class file.
 * 
 * This class represents third party related website urls for this card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallCardPurchase
{
	
	/**
	 * The url to the tcgplayer website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayer = null;
	
	/**
	 * The url to the cardmarket website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardmarket = null;
	
	/**
	 * The url to the cardhoarder website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardhoarder = null;
	
}
