<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallPreview class file.
 * 
 * This class represents the preview data of a given card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallPreview
{
	
	/**
	 * The date this card was previewed.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $previewedAt = null;
	
	/**
	 * A link to the preview for this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $sourceUri = null;
	
	/**
	 * The name of the source that previewed this card.
	 * 
	 * @var ?string
	 */
	public ?string $source = null;
	
}
