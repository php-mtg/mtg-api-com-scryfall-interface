<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallCard class file.
 *
 * This class represents a card from the scryfall api.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComScryfallCard
{
	
	// ---- ---- core card fields ---- ---- \\
	
	/**
	 * A unique ID for this card in Scryfall’s database.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $id = null;
	
	/**
	 * This card’s Arena ID, if any. A large percentage of cards are not
	 * available on Arena and do not have this ID. 
	 * 
	 * @var ?integer
	 */
	public ?int $arenaId = null;
	
	/**
	 * A language code for this printing.
	 * 
	 * @var ?string
	 */
	public ?string $lang = null;
	
	/**
	 * This card’s Magic Online ID (also known as the Catalog ID), if any.
	 * A large percentage of cards are not available on Magic Online and do
	 * not have this ID. 
	 * 
	 * @var ?integer
	 */
	public ?int $mtgoId = null;
	
	/**
	 * This card’s foil Magic Online ID (also known as the Catalog ID), if any.
	 * A large percentage of cards are not available on Magic Online and do
	 * not have this ID. 
	 * 
	 * @var ?integer
	 */
	public ?int $mtgoFoilId = null;
	
	/**
	 * This card’s multiverse IDs on Gatherer, if any, as an array of integers.
	 * Note that Scryfall includes many promo cards, tokens, and other esoteric
	 * objects that do not have these identifiers.
	 * 
	 * @var array<integer, integer>
	 */
	public array $multiverseIds = [];
	
	/**
	 * This card’s ID on TCGplayer’s API, also known as the productId.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerId = null;
	
	/**
	 * This card's ID on TCGplayer's API for the etched version.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerEtchedId = null;
	
	/**
	 * This card’s ID on Cardmarket’s API, also known as the idProduct.
	 * 
	 * @var ?integer
	 */
	public ?int $cardmarketId = null;
	
	/**
	 * A unique ID for this card’s oracle identity. This value is consistent
	 * across reprinted card editions, and unique among different cards with
	 * the same name (tokens, Unstable variants, etc).
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $oracleId = null;
	
	/**
	 * A link to where you can begin paginating all re/prints for this card
	 * on Scryfall’s API. 
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $printsSearchUri = null;
	
	/**
	 * A link to this card’s rulings list on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $rulingsUri = null;
	
	/**
	 * A link to this card’s permapage on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $scryfallUri = null;
	
	/**
	 * A link to this card object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $uri = null;
	
	
	// ---- ---- gameplay fields ---- ---- \\
	
	/**
	 * If this card is closely related to other cards, this property will be
	 * an array with Related Card Objects.
	 * 
	 * @var array<integer, ApiComScryfallRelatedCard>
	 */
	public array $allParts = [];
	
	/**
	 * An array of Card Face objects, if this card is multifaced.
	 * 
	 * @var array<integer, ApiComScryfallCardFace>
	 */
	public array $cardFaces = [];
	
	/**
	 * The card’s converted mana cost. Note that some funny cards have
	 * fractional mana costs. 
	 * 
	 * @var ?float
	 */
	public ?float $cmc = null;
	
	/**
	 * This card’s colors, if the overall card has colors defined by the rules.
	 * Otherwise the colors will be on the card_faces objects, see below.
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * This card’s color identity.
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIdentity = [];
	
	/**
	 * The colors in this card’s color indicator, if any. A null value for
	 * this field indicates the card does not have one.
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIndicator = [];
	
	/**
	 * This card’s overall rank/popularity on EDHREC. Not all cards are ranked.
	 * 
	 * @var ?integer
	 */
	public ?int $edhrecRank = null;
	
	/**
	 * True if this printing exists in a foil version.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The finishes of this card.
	 * 
	 * @var array<integer, string>
	 */
	public array $finishes = [];
	
	/**
	 * This card’s hand modifier, if it is Vanguard card. This value will
	 * contain a delta, such as -1.
	 * 
	 * @var ?string
	 */
	public ?string $handModifier = null;
	
	/**
	 * An array of keywords that this card uses, such as 'Flying' and
	 * 'Cumulative upkeep'.
	 * 
	 * @var array<integer, string>
	 */
	public array $keywords = [];
	
	/**
	 * The produced mana, if this card produces any.
	 * 
	 * @var array<integer, string>
	 */
	public array $producedMana = [];
	
	/**
	 * A code for this card’s layout.
	 * 
	 * @var ?string
	 */
	public ?string $layout = null;
	
	/**
	 * An object describing the legality of this card across play formats.
	 * Possible legalities are legal, not_legal, restricted, and banned.
	 * 
	 * @var ?ApiComScryfallCardLegality
	 */
	public ?ApiComScryfallCardLegality $legalities = null;
	
	/**
	 * This card’s life modifier, if it is Vanguard card. This value will
	 * contain a delta, such as +2.
	 * 
	 * @var ?string
	 */
	public ?string $lifeModifier = null;
	
	/**
	 * This loyalty if any. Note that some cards have loyalties that are not
	 * numeric, such as X.
	 * 
	 * @var ?string
	 */
	public ?string $loyalty = null;
	
	/**
	 * The mana cost for this card. This value will be any empty string "" if
	 * the cost is absent. Remember that per the game rules, a missing mana
	 * cost and a mana cost of {0} are different values. Multi-faced cards
	 * will report this value in card faces.
	 * 
	 * @var ?string
	 */
	public ?string $manaCost = null;
	
	/**
	 * The name of this card. If this card has multiple faces, this field will
	 * contain both names separated by _ // _ .
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * True if this printing exists in a nonfoil version.
	 * 
	 * @var ?boolean
	 */
	public ?bool $nonfoil = null;
	
	/**
	 * The Oracle text for this card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $oracleText = null;
	
	/**
	 * True if this card is oversized.
	 * 
	 * @var ?boolean
	 */
	public ?bool $oversized = null;
	
	/**
	 * This card’s rank/popularity on Penny Dreadful. Not all cards are ranked.
	 * 
	 * @var ?integer
	 */
	public ?int $pennyRank = null;
	
	/**
	 * This card’s power, if any. Note that some cards have powers that are
	 * not numeric, such as *.
	 * 
	 * @var ?string
	 */
	public ?string $power = null;
	
	/**
	 * True if this card is on the Reserved List.
	 * 
	 * @var ?boolean
	 */
	public ?bool $reserved = null;
	
	/**
	 * The security stamp value (normal or acorn).
	 * 
	 * @var ?string
	 */
	public ?string $securityStamp = null;
	
	/**
	 * This card’s toughness, if any. Note that some cards have toughnesses
	 * that are not numeric, such as *.
	 * 
	 * @var ?string
	 */
	public ?string $toughness = null;
	
	/**
	 * The type line of this card.
	 * 
	 * @var ?string
	 */
	public ?string $typeLine = null;
	
	
	// ---- ---- print fields ---- ---- \\
	
	/**
	 * The id of the artists.
	 * 
	 * @var array<integer, string>
	 */
	public array $artistIds = [];
	
	/**
	 * The name of the illustrator of this card. Newly spoiled cards may not
	 * have this field yet.
	 * 
	 * @var ?string
	 */
	public ?string $artist = null;
	
	/**
	 * 	Whether this card is found in boosters.
	 * 
	 * @var ?boolean
	 */
	public ?bool $booster = null;
	
	/**
	 * This card’s border color: black, borderless, gold, silver, or white.
	 * 
	 * @var ?string
	 */
	public ?string $borderColor = null;
	
	/**
	 * The Scryfall ID for the card back design present on this card.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $cardBackId = null;
	
	/**
	 * This card’s collector number. Note that collector numbers can contain
	 * non-numeric characters, such as letters or ★.
	 * 
	 * @var ?string
	 */
	public ?string $collectorNumber = null;
	
	/**
	 * True if this is a digital card on Magic Online.
	 * 
	 * @var ?boolean
	 */
	public ?bool $digital = null;
	
	/**
	 * The flavor text, if any.
	 * 
	 * @var ?string
	 */
	public ?string $flavorText = null;
	
	/**
	 * This card’s frame effects, if any.
	 * 
	 * legendary			The cards have a legendary crown
	 * miracle				The miracle frame effect
	 * nyxtouched			The Nyx-touched frame effect
	 * draft				The draft-matters frame effect
	 * devoid				The Devoid frame effect
	 * tombstone			The Odyssey tombstone mark
	 * colorshifted			A colorshifted frame
	 * inverted				The FNM-style inverted frame
	 * sunmoondfc			The sun and moon transform marks
	 * compasslanddfc		The compass and land transform marks
	 * originpwdfc			The Origins and planeswalker transform marks
	 * mooneldrazidfc		The moon and Eldrazi transform marks
	 * moonreversemoondfc	The waxing and waning crescent moon transform marks
	 * showcase				A custom Showcase frame
	 * extendedart			An extended art frame
	 * 
	 * @var array<integer, string>
	 */
	public array $frameEffects = [];
	
	/**
	 * This card’s frame layout.
	 * 
	 * 1993		The original Magic card frame, starting from Limited Edition Alpha.
	 * 1997		The updated classic frame starting from Mirage block
	 * 2003		The “modern” Magic card frame, introduced in Eighth Edition and Mirrodin block.
	 * 2015		The holofoil-stamp Magic card frame, introduced in Magic 2015.
	 * future	The frame used on cards from the future
	 * 
	 * @var ?string
	 */
	public ?string $frame = null;
	
	/**
	 * True if this card’s artwork is larger than normal.
	 * 
	 * @var ?boolean
	 */
	public ?bool $fullArt = null;
	
	/**
	 * A list of games that this card print is available in, paper, arena,
	 * and/or mtgo.
	 * 
	 * @var array<integer, string>
	 */
	public array $games = [];
	
	/**
	 * True if this card’s imagery is high resolution.
	 * 
	 * @var ?boolean
	 */
	public ?bool $highresImage = null;
	
	/**
	 * Gets the value of the status of the image (like "highres_scan").
	 * 
	 * @var ?string
	 */
	public ?string $imageStatus = null;
	
	/**
	 * A unique identifier for the card artwork that remains consistent across
	 * reprints. Newly spoiled cards may not have this field yet.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $illustrationId = null;
	
	/**
	 * An object listing available imagery for this card. See the Card Imagery
	 * article for more information.
	 * 
	 * @var ?ApiComScryfallCardImagery
	 */
	public ?ApiComScryfallCardImagery $imageUris = null;
	
	/**
	 * An object containing daily price information for this card, including
	 * usd, usd_foil, eur, and tix prices, as strings.
	 * 
	 * @var ?ApiComScryfallPrices
	 */
	public ?ApiComScryfallPrices $prices = null;
	
	/**
	 * The localized name printed on this card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedName = null;
	
	/**
	 * The localized text printed on this card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedText = null;
	
	/**
	 * The localized type line printed on this card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $printedTypeLine = null;
	
	/**
	 * True if this card is a promotional print.
	 * 
	 * @var ?boolean
	 */
	public ?bool $promo = null;
	
	/**
	 * An array of strings describing what categories of promo cards this card
	 * falls into.
	 * 
	 * @var array<integer, string>
	 */
	public array $promoTypes = [];
	
	/**
	 * An object providing URIs to this card’s listing on major marketplaces.
	 * 
	 * @var ?ApiComScryfallCardPurchase
	 */
	public ?ApiComScryfallCardPurchase $purchaseUris = null;
	
	/**
	 * This card’s rarity. One of common, uncommon, rare, or mythic.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * An object providing URIs to this card’s listing on other Magic: The
	 * Gathering online resources.
	 * 
	 * @var ?ApiComScryfallCardResource
	 */
	public ?ApiComScryfallCardResource $relatedUris = null;
	
	/**
	 * The date this card was first released.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releasedAt = null;
	
	/**
	 * True if this card is a reprint.
	 * 
	 * @var ?boolean
	 */
	public ?bool $reprint = null;
	
	/**
	 * A link to this card’s set on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $scryfallSetUri = null;
	
	/**
	 * This card’s set code.
	 * 
	 * @var ?string
	 */
	public ?string $setId = null;
	
	/**
	 * This card's set code.
	 * 
	 * @var ?string
	 */
	public ?string $set = null;
	
	/**
	 * This card’s full set name.
	 * 
	 * @var ?string
	 */
	public ?string $setName = null;
	
	/**
	 * A link to where you can begin paginating this card’s set on the
	 * Scryfall API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $setSearchUri = null;
	
	/**
	 * 	The type of set this printing is in.
	 * 
	 * @var ?string
	 */
	public ?string $setType = null;
	
	/**
	 * A link to this card’s set object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $setUri = null;
	
	/**
	 * True if this card is a Story Spotlight.
	 * 
	 * @var ?boolean
	 */
	public ?bool $storySpotlight = null;
	
	/**
	 * True if the card is printed without text.
	 * 
	 * @var ?boolean
	 */
	public ?bool $textless = null;
	
	/**
	 * 	Whether this card is a variation of another printing.
	 * 
	 * @var ?boolean
	 */
	public ?bool $variation = null;
	
	/**
	 * The printing ID of the printing this card is a variation of.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $variationOf = null;
	
	/**
	 * This card’s watermark, if any.
	 * 
	 * @var ?string
	 */
	public ?string $watermark = null;
	
	/**
	 * The preview data for this card.
	 * 
	 * @var ?ApiComScryfallPreview
	 */
	public ?ApiComScryfallPreview $preview = null;
	
}
