<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComScryfall;

use Psr\Http\Message\UriInterface;

/**
 * ApiComScryfallCardImagery class file.
 * 
 * This class represents the image links of a given card.
 * 
 * @author Anastaszor
 */
class ApiComScryfallCardImagery
{
	
	/**
	 * The url to the small image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $small = null;
	
	/**
	 * The url to the normal image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $normal = null;
	
	/**
	 * The url to the large image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $large = null;
	
	/**
	 * The url to the png image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $png = null;
	
	/**
	 * The url to the art crop image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $artCrop = null;
	
	/**
	 * The url to the border crop image of this card.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $borderCrop = null;
	
}
